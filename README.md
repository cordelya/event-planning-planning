# Event Planning Planning

We have an event-planning gitlab template. We also need a project to manage the planning for that project. That's what will happen here.

## Getting started

If you are new to GitLab: Creating a free Gitlab account will allow you to add new Issues and comment on those already in progress. If you're participating as a member of an organization and you have an organizational email address, use that to create your account. If you normally log in to your organizational email via Gmail, you can use the "Log in with Google" account creation/sign-on option instead of creating a password.

## Advanced Moves

If you're experienced in git / GitLab / GitHub operations, you can fork the Planning Template and submit Merge Requests back via that method. Always feel free to discuss your proposed changes in the issues here before you do, so you don't do a bunch of work that we end up not accepting.
